// Fill out your copyright notice in the Description page of Project Settings.


#include "DummyForKill.h"
#include "Components/StaticMeshComponent.h"
#include "Engine.h"

ADummyForKill::ADummyForKill()
{
	UStaticMeshComponent* StaticMeshComponent = GetStaticMeshComponent();
	
	StaticMeshComponent->SetNotifyRigidBodyCollision(true);
	StaticMeshComponent->BodyInstance.SetCollisionProfileName("BlockAllDynamic");
	StaticMeshComponent->OnComponentHit.AddDynamic(this, &ADummyForKill::OnCompHit);
	
	CurrentHealth = MaxHealth;
}

void ADummyForKill::OnCompHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	if (CurrentHealth <= 0)
	{
		Destroy();
	}
}


