// Fill out your copyright notice in the Description page of Project Settings.


#include "MyGameInstance.h"

void UMyGameInstance::AddScore(int ScoreToAdd)
{
	CurrentScore += ScoreToAdd;
}

int UMyGameInstance::GetCurrentScore() const
{
	return CurrentScore;
}

void UMyGameInstance::FlushCurrentScore()
{
	CurrentScore = 0;
}
