// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "BasePickUpItem.generated.h"


UCLASS(abstract)
class HWMASTER_API ABasePickUpItem : public AStaticMeshActor
{
	GENERATED_BODY()
public:
	ABasePickUpItem();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseConfig|Effects")
		class UParticleSystem* Particles;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseConfig|Effects")
		class USoundBase* ItemSound;
	
	UPROPERTY()
		FTimerHandle TimerEffect;

public:
	virtual void BeginPlay();
	virtual void OnDestroyed();
	virtual void OverlapItem(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	


};
