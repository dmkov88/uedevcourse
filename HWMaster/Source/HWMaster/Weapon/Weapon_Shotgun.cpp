// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon_Shotgun.h"
#include "Public/CollisionQueryParams.h"
#include "Engine.h"
#include "Kismet/KismetSystemLibrary.h"
#include "DummyForKill.h"
#include "Weapon/ImpactEffectComponent.h"

void AWeapon_Shotgun::WeaponFire()
{
	
	if (CurrentAmmo > 0)
	{
		Super::WeaponFire();
		for (int32 i = 0; i <= WeaponSpread; i++)
		{
			Instant_Fire();
		}
	}
}

void AWeapon_Shotgun::BeginPlay()
{
	Super::BeginPlay();
}

FHitResult AWeapon_Shotgun::WeaponTrace(const FVector & TraceFrom, const FVector & TraceTo) const
{
	static FName WeaponFireTag = FName(TEXT("WeaponTrace"));
	FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
	TraceParams.bTraceComplex = true;
	TraceParams.bReturnPhysicalMaterial = true;
	TraceParams.AddIgnoredActor(this);

	FHitResult Hit(ForceInit);
	
	GetWorld()->LineTraceSingleByChannel(Hit, TraceFrom, TraceTo, TRACE_WEAPON, TraceParams);

	return Hit;
}

void AWeapon_Shotgun::ProcessInstantHit(const FHitResult & Impact, const FVector & Origin, const FVector & ShootDir, int32 RandomSeed, float ReticleSpread)
{
	const FVector EndTrace = Origin + ShootDir * WeaponRange;
	const FVector EndPoint = Impact.GetActor() ? Impact.ImpactPoint : EndTrace;
	//DrawDebugLine(this->GetWorld(), Origin, Impact.TraceEnd, FColor::Green, true, 0.01f, 0.01f);
	
	ImpactEffectComp->SpawnImpactEffect(Impact);

	ADummyForKill *TargetActor = Cast<ADummyForKill>(Impact.GetActor());
	if (TargetActor)
	{
		TargetActor->CurrentHealth -= MaxDamage;
		
	}
}


void AWeapon_Shotgun::Instant_Fire()
{
	const int32 RandomSeed = FMath::Rand();
	FRandomStream WeaponRandomStream(RandomSeed);
	const float CurrentSpread = WeaponSpread;
	const float SpreadCone = FMath::DegreesToRadians(WeaponSpread * 0.5);
	const FVector AimDir = GetStaticMeshComponent()->GetSocketRotation(MuzzleSocketName).Vector();
	const FVector StartTrace = GetStaticMeshComponent()->GetSocketLocation(MuzzleSocketName);
	const FVector ShootDir = WeaponRandomStream.VRandCone(AimDir, SpreadCone, SpreadCone);
	const FVector EndTrace = StartTrace + ShootDir * WeaponRange;
	const FHitResult Impact = WeaponTrace(StartTrace, EndTrace);

	ProcessInstantHit(Impact, StartTrace, ShootDir, RandomSeed, CurrentSpread);
}

void AWeapon_Shotgun::WeaponReload()
{
	Super::WeaponReload();
	
}
