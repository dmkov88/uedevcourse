// Fill out your copyright notice in the Description page of Project Settings.


#include "VitalityComponent.h"
#include "BaseAICharacter.h"
#include "Engine.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "MyGameInstance.h"




// Sets default values for this component's properties
UVitalityComponent::UVitalityComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


float UVitalityComponent::GetCurrentHealth()
{
	return CurrentHealth;
}

// Called when the game starts
void UVitalityComponent::BeginPlay()
{
	Super::BeginPlay();
	if (GetOwner())
	{
		GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UVitalityComponent::DamageHandle);
	}
}


void UVitalityComponent::DamageHandle(AActor * DamagedActor, float Damage, const UDamageType * DamageType, AController * InstigatedBy, AActor * DamageCauser)
{
	if (!isAlive())
	{
		return;
	}
	
	CurrentHealth -= Damage;
	OnHealthChanged.Broadcast();

	if (!isAlive())
	{
		OmOwnerDied.Broadcast();
			
		UMyGameInstance * MyInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
		MyInstance->AddScore(Score);

		ABaseAICharacter* AiEnemy = Cast<ABaseAICharacter>(DamagedActor);

		if (AiEnemy)
		{
			AiEnemy->AIDieCharacter();
		}
		
	}

	GetWorld()->GetTimerManager().SetTimer(RegenerationTimer, this, &UVitalityComponent::HealthRegeneration, RegenerationSpped, true, TimerDelay);
}

void UVitalityComponent::HealthRegeneration()
{
	if (CurrentHealth != MaxHealth && CurrentHealth >0 )
	{
		CurrentHealth > MaxHealth ? CurrentHealth = MaxHealth : CurrentHealth += Regeneration;
		OnHealthRegeneration.Broadcast();
	}
	else
	{
		GetWorld()->GetTimerManager().ClearTimer(RegenerationTimer);
	}
}

// Called every frame
void UVitalityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UVitalityComponent::isAlive()
{
	return CurrentHealth > 0;
}

