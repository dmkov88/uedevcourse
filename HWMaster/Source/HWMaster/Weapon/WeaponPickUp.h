// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/BasePickUpItem.h"
#include "WeaponPickUp.generated.h"

/**
 * 
 */
UCLASS(abstract)
class HWMASTER_API AWeaponPickUp : public ABasePickUpItem
{
	GENERATED_BODY()
	
	AWeaponPickUp();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Setup")
		int AmmoPoolCost = 7;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Setup")
		TSubclassOf<class ABaseWeapon> WeaponItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Setup")
		class UBoxComponent* BoxCollider;
	
	UPROPERTY(EditDefaultsOnly, Category = "Item Setup")
		class USphereComponent* SphereCollider;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Setup")
		class UStaticMeshComponent* ColiderMesh;


public:
	UFUNCTION()
		void GiveAmmo(AMyCharacter * OverlapedCharacter, int AmmoCount);

	virtual void OnDestroyed() override;
	
	UFUNCTION(BlueprintCallable)
	virtual void OverlapItem(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

};
