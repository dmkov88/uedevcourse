// Fill out your copyright notice in the Description page of Project Settings.


#include "MyCharacter.h"
#include "Engine/World.h"
#include "Animation/AnimBlueprint.h"
#include "Weapon/BaseWeapon.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "ConstructorHelpers.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "VitalityComponent.h"


// Sets default values
AMyCharacter::AMyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	VitalityComponent = CreateDefaultSubobject<UVitalityComponent>(TEXT("VitalityComponent"));

	AutoPossessPlayer = EAutoReceiveInput::Player0;

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArmComponent->SetupAttachment(RootComponent);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CharacterCamera"));
	CameraComponent->SetupAttachment(SpringArmComponent);

	if (SpringArmComponent)
	{
		SpringArmComponent->AddLocalTransform(FTransform(FRotator(-90, 0, 0).Quaternion()));
		SpringArmComponent->TargetArmLength = 600;
	}

	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SkeletalMesh(TEXT("SkeletalMesh'/Game/AnimStarterPack/UE4_Mannequin/Mesh/SK_Mannequin.SK_Mannequin'"));
	const ConstructorHelpers::FObjectFinder<UAnimBlueprint> AnimObj(TEXT("AnimBlueprint'/Game/AnimStarterPack/UE4_Mannequin/Mesh/ABP_CharacterAnim.ABP_CharacterAnim'"));

	if (SkeletalMesh.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SkeletalMesh.Object);
		GetMesh()->AddLocalTransform(FTransform(FRotator(0, -90, 0).Quaternion(), FVector(0, 0, -90)));
		GetMesh()->SetAnimInstanceClass(AnimObj.Object->GeneratedClass);
	}

	bUseControllerRotationYaw = false;
	SpringArmComponent->bInheritPitch = false;
	SpringArmComponent->bInheritRoll = false;
	SpringArmComponent->bInheritYaw = false;

	JogCharacter();

	VitalityComponent->OmOwnerDied.AddDynamic(this, &AMyCharacter::CharacterDied);
}

// Called when the game starts or when spawned
void AMyCharacter::BeginPlay()
{
	Super::BeginPlay();

	CurrentWeapon = SpawnWeapon();
	AttachWeapon(WeaponSocket, CurrentWeapon);
	
}

void AMyCharacter::CharacterDied()
{
	GetCharacterMovement()->DisableMovement();
	bUseControllerRotationYaw = false;
	StopFIre();
	/*Disable player fire after death*/
	CurrentWeapon = nullptr;
}

void AMyCharacter::MoveForward(float Scale)
{
	AddMovementInput(FVector(1, 0, 0), Scale);
}

void AMyCharacter::Strafe(float Scale)
{
	AddMovementInput(FVector(0, 1, 0), Scale);
}

void AMyCharacter::WalkCharacter()
{
	GetCharacterMovement()->MaxWalkSpeed = CharacterSpeed::Walk;
}

void AMyCharacter::SprintCharacter()
{
	GetCharacterMovement()->MaxWalkSpeed = CharacterSpeed::Sprint;
}

void AMyCharacter::JogCharacter()
{
	GetCharacterMovement()->MaxWalkSpeed = CharacterSpeed::Jog;
}

void AMyCharacter::StartFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StartFire();
	}
}

void AMyCharacter::StopFIre()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
		
	}
}

void AMyCharacter::Reload()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->WeaponReload();
	}
}

void AMyCharacter::WeaponFireRate()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->bAutoFire = !(CurrentWeapon->bAutoFire);
		ChangeFireRate.Broadcast();
	}
}

void AMyCharacter::CameraZoomIn()
{
	if (SpringArmComponent->TargetArmLength <= MaxCameraZoom)
	{
		SpringArmComponent->TargetArmLength += CameraZoomStep;
	}
}

void AMyCharacter::CameraZoomOut()
{
	if (SpringArmComponent->TargetArmLength >= MinCameraZoom)
	{
		SpringArmComponent->TargetArmLength -= CameraZoomStep;
	}
}

ABaseWeapon * AMyCharacter::SpawnWeapon()
{
	if (WeaponToSpawn)
	{
		FTransform TmpTransform = FTransform::Identity;
		FActorSpawnParameters SpawnParametrs;
		SpawnParametrs.Owner = this;
		return Cast<ABaseWeapon>(GetWorld()->SpawnActor(WeaponToSpawn, &TmpTransform, SpawnParametrs));
	}
	
	return nullptr;
}

void AMyCharacter::AttachWeapon(FName SocketName, ABaseWeapon * WeaponAttach)
{
	if (WeaponAttach)
	{
		WeaponAttach->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, SocketName);
	}
}

// Called every frame
void AMyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	RotateMouse();

}

FName AMyCharacter::GetWeaponSocket()
{
	return WeaponSocket;
}

void AMyCharacter::RotateMouse()
{
	FHitResult Hit;
	float RotationYaw;

	if (!VitalityComponent->isAlive())
	{
		return;
	}

	if (GetWorld()->GetFirstPlayerController()->GetHitResultUnderCursor(ECC_Visibility, false, Hit))
	{
		RotationYaw = FRotationMatrix::MakeFromX(Hit.ImpactPoint - GetActorLocation()).Rotator().Yaw;
	}
	else
	{
		FVector mouseWorldLocation, mouseWorldDirection;

		GetWorld()->GetFirstPlayerController()->DeprojectMousePositionToWorld(mouseWorldLocation, mouseWorldDirection);
		RotationYaw = mouseWorldDirection.Rotation().Yaw;
	}

	SetActorRotation(FRotator(0, RotationYaw, 0));
}

void AMyCharacter::EquipWeapon(TSubclassOf<class ABaseWeapon> WeaponToEquip, FName SocketName)
{
	
	CurrentWeapon->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	CurrentWeapon->Destroy();
	FTransform TmpTransform = FTransform::Identity;
	FActorSpawnParameters SpawnParametrs;
	SpawnParametrs.Owner = this;
	CurrentWeapon = Cast<ABaseWeapon>(GetWorld()->SpawnActor(WeaponToEquip, &TmpTransform, SpawnParametrs));
	this->AttachWeapon(SocketName, CurrentWeapon);
	UE_LOG(LogTemp, Warning, TEXT("%s"), *CurrentWeapon->GetName());

	WeaponEquipedDlg.Broadcast();
}

// Called to bind functionality to input
void AMyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	if (!bDisableMovement)
	{
		PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AMyCharacter::MoveForward);
		PlayerInputComponent->BindAxis(TEXT("Strafe"), this, &AMyCharacter::Strafe);
	}

	PlayerInputComponent->BindAction(TEXT("Walk"), IE_Pressed, this, &AMyCharacter::WalkCharacter);
	PlayerInputComponent->BindAction(TEXT("Walk"), IE_Released, this, &AMyCharacter::JogCharacter);
	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Pressed, this, &AMyCharacter::SprintCharacter);
	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Released, this, &AMyCharacter::JogCharacter);

	PlayerInputComponent->BindAction(TEXT("CameraZoomUp"), IE_Pressed, this, &AMyCharacter::CameraZoomIn);
	PlayerInputComponent->BindAction(TEXT("CameraZoomOut"), IE_Pressed, this, &AMyCharacter::CameraZoomOut);

	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &AMyCharacter::StartFire);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Released, this, &AMyCharacter::StopFIre);
	PlayerInputComponent->BindAction(TEXT("ReloadWeapon"), IE_Pressed, this, &AMyCharacter::Reload);
	PlayerInputComponent->BindAction(TEXT("ChangeFireRate"), IE_Released, this, &AMyCharacter::WeaponFireRate);
}


