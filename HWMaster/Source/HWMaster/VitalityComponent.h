// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VitalityComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParDelegate);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class HWMASTER_API UVitalityComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UVitalityComponent();

	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = "Health")
		float MaxHealth = 100.f;

	UFUNCTION(BlueprintCallable)
		float GetCurrentHealth();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Score")
		int Score = 100;

	UPROPERTY(BlueprintReadOnly, Category = "Health")
		float CurrentHealth = MaxHealth;

	UPROPERTY(BlueprintReadOnly, Category = "Health")
		float Regeneration = 0.01f;

	UPROPERTY(BlueprintReadOnly, Category = "Health")
		float RegenerationSpped = 0.001f;

	UPROPERTY(BlueprintReadOnly, Category = "Health")
		float TimerDelay = 2.f;

	UFUNCTION()
		void DamageHandle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION()
		void HealthRegeneration();

	UPROPERTY()
		FTimerHandle RegenerationTimer;

	UPROPERTY(BlueprintAssignable)
		FNoParDelegate OnHealthChanged;
	
	UPROPERTY(BlueprintAssignable)
		FNoParDelegate OnHealthRegeneration;
	

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintAssignable)
	FNoParDelegate OmOwnerDied;

	UFUNCTION(BlueprintCallable)
		bool isAlive();


};
