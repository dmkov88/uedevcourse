// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ImpactEffectActor.generated.h"

UCLASS(abstract)
class HWMASTER_API AImpactEffectActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AImpactEffectActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		class UMaterialInterface* DecalMaterial;
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		class USoundBase* EffectSound;
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		class UParticleSystem* EffectParticles;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		bool bApplyImpulse = true;
	UPROPERTY(EditDefaultsOnly, Category = "Effects", meta = (EditCondition="bApplyImpulse"))
		float ImpulseStregth = 300;


	void SpawnEffects();

	FHitResult EffectHit;

public:	
	UPROPERTY(EditAnywhere, Category = "Damage")
		float Damage = 10;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void HitInint(FHitResult Hit);

	UFUNCTION()
		float GetDamage();

	
};
