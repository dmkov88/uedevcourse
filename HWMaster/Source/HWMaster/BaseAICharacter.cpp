// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseAICharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "VitalityComponent.h"
#include "TimerManager.h"


// Sets default values
ABaseAICharacter::ABaseAICharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	VitalityComponent = CreateDefaultSubobject<UVitalityComponent>(TEXT("VitalityComponent"));

	//bIsDead = false;
	if (VitalityComponent)
	{
		VitalityComponent->OmOwnerDied.AddDynamic(this, &ABaseAICharacter::AIDieCharacter);
	}


}

void ABaseAICharacter::AIDieCharacter()
{
	if (VitalityComponent)
	{
		if (VitalityComponent->GetCurrentHealth() <= 0)
		{
			UE_LOG(LogTemp, Warning, TEXT("Dead"));
			GetCharacterMovement()->StopMovementImmediately();
			bIsDead = true;
			GetWorld()->GetTimerManager().SetTimer(AITimerHandle, this, &ABaseAICharacter::DestroyActor, 0.1f, false, 2.3f);
			
		}
	}
}


// Called when the game starts or when spawned
void ABaseAICharacter::BeginPlay()
{
	Super::BeginPlay();

}

void ABaseAICharacter::AIMoveWalk()
{
	GetCharacterMovement()->MaxWalkSpeed = AIMovementSpeed::AiWalk;
}

void ABaseAICharacter::AIMoveJog()
{
	GetCharacterMovement()->MaxWalkSpeed = AIMovementSpeed::AiJog;
}

void ABaseAICharacter::AIMoveSprint()
{
	GetCharacterMovement()->MaxWalkSpeed = AIMovementSpeed::AiWalk;
}

void ABaseAICharacter::DestroyActor()
{
	Destroy();
}

// Called every frame
void ABaseAICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABaseAICharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

