// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/BaseWeapon.h"
#include "Weapon_Shotgun.generated.h"

#define TRACE_WEAPON ECC_GameTraceChannel1
/**
 *
 */
UCLASS(abstract)
class HWMASTER_API AWeapon_Shotgun : public ABaseWeapon
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Deault")
		float MaxDamage;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Deault")
		float WeaponSpread;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float WeaponRange;


	virtual void WeaponFire() override;
	virtual void BeginPlay() override;

	FHitResult WeaponTrace(const FVector &TraceFrom, const FVector &TraceTo) const;
	void ProcessInstantHit(const FHitResult &Impact, const FVector &Origin, const FVector &ShootDir, int32 RandomSeed, float ReticleSpread);

private:
	UFUNCTION()
	void Instant_Fire();

public:
	virtual void WeaponReload() override;

};
