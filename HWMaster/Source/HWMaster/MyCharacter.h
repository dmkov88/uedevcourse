// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MyCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEquipedWeapon);

class UCameraComponent;
class USpringArmComponent;
class UCharacterMovementComponent;

UENUM()
enum CharacterSpeed
{
	Walk = 250,
	Jog = 600,
	Sprint = 1200
};


UCLASS()
class HWMASTER_API AMyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void CharacterDied();

	void MoveForward(float Scale);
	void Strafe(float Scale);
	void WalkCharacter();
	void SprintCharacter();
	void JogCharacter();

	void StartFire();
	void StopFIre();
	void Reload();
	void WeaponFireRate();

	void CameraZoomIn();
	void CameraZoomOut();

	class ABaseWeapon* SpawnWeapon();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Settings")
		float MaxCameraZoom = 1200;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Settings")
		float MinCameraZoom = 600;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USpringArmComponent* SpringArmComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UVitalityComponent* VitalityComponent;

	void AttachWeapon(FName SocketName, ABaseWeapon * WeaponAttach);

	
	UPROPERTY()
		FName WeaponSocket;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		FName GetWeaponSocket();

	void RotateMouse();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Global state")
		bool bDisableMovement;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera controls")
		float CameraZoomStep = 40;

	UPROPERTY(BlueprintReadOnly)
		class ABaseWeapon* CurrentWeapon;
	
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class ABaseWeapon> WeaponToSpawn;

	UFUNCTION()
		void EquipWeapon(TSubclassOf<class ABaseWeapon> WeaponToEquip, FName SocketName);

	UPROPERTY(BlueprintAssignable)
		FEquipedWeapon WeaponEquipedDlg;

	UPROPERTY(BlueprintAssignable)
		FEquipedWeapon ChangeFireRate;
};
