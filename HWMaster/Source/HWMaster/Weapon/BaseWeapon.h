// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/Texture2D.h"
#include "Engine/StaticMeshActor.h"
#include "BaseWeapon.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDlgNoParam);

UCLASS(abstract)
class HWMASTER_API ABaseWeapon : public AStaticMeshActor
{
	GENERATED_BODY()


protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseConfig|Ammo")
		int AmmoPool;
	
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseConfig|Defaults", meta=(EditCondition = "bAutoFire"))
		float FireRange = 100;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseConfig|Defaults")
		int32 Priority;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseConfig|Defaults")
		FName MuzzleSocketName;
	
	UPROPERTY(BlueprintReadOnly)
		int CurrentAmmo;

	UPROPERTY()
		FTimerHandle AutoFireTimerTH;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseConfig|Defaults")
		class USoundBase* EffectSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseConfig|Defaults")
		class UParticleSystem* EffectParticles;
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseConfig|Defaults")
		class UAnimMontage* FireAnimation;

	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseConfig|Defaults")
		class UAnimMontage* ReloadAnimation;

	UPROPERTY()
		class ACharacter * WeaponOwner;*/

	UFUNCTION()
		int AmmoPoolRealod(int &APool, int &MAmmo);

	UPROPERTY(VisibleAnywhere)
		class UImpactEffectComponent* ImpactEffectComp;
	

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseConfig|Defaults")
		class UAnimMontage* ReloadAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseConfig|Defaults")
		UTexture2D * WeaponIcon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseConfig|Defaults")
		UTexture2D * FireRateLowIcon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseConfig|Defaults")
		UTexture2D * FireRateHighIcon;

	UPROPERTY()
		class ACharacter * WeaponOwner;

	UPROPERTY(BlueprintReadWrite, Category = "BaseConfig|Ammo")
		UTexture2D* WeaponImage;

	ABaseWeapon();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseConfig|Defaults")
		bool bAutoFire = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseConfig|Ammo")
		int32 MaxAmmo;

	UFUNCTION()
		virtual void StartFire();
	UFUNCTION()
		virtual void StopFire();
	UFUNCTION()
		virtual void WeaponReload();

	UPROPERTY(BlueprintAssignable)
		FDlgNoParam NoAmmoDelegate;

	UPROPERTY(BlueprintAssignable)
		FDlgNoParam AmmoChange;

	UPROPERTY(BlueprintAssignable)
		FDlgNoParam WeaponReloaded;


protected:

	UFUNCTION()
		virtual void WeaponFire();
	
	
		FTransform GetMuzzleTransfor();

	virtual void BeginPlay() override;

};
