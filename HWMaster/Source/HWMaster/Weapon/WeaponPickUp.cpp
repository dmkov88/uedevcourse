// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponPickUp.h"
#include "Engine.h"
#include "MyCharacter.h"
#include "BaseWeapon.h"

AWeaponPickUp::AWeaponPickUp()
{
	
	ColiderMesh = CreateDefaultSubobject<UStaticMeshComponent>("SphereCollider");
	ColiderMesh->SetupAttachment(RootComponent);
	ColiderMesh->OnComponentBeginOverlap.AddDynamic(this, &AWeaponPickUp::OverlapItem);
	
}

void AWeaponPickUp::GiveAmmo(AMyCharacter * OverlapedCharacter, int AmmoCount)
{
	OverlapedCharacter->CurrentWeapon->MaxAmmo += AmmoCount;
}



void AWeaponPickUp::OnDestroyed()
{

}

void AWeaponPickUp::OverlapItem(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("Overlaped"));
	AMyCharacter * TmpCharacter = Cast<AMyCharacter>(OtherActor);

	if (TmpCharacter)
	{
		ABaseWeapon * TmpCharWeapon = TmpCharacter->CurrentWeapon;
		if (TmpCharWeapon->GetClass()->GetName() == WeaponItem->GetName())
		{
			GiveAmmo(TmpCharacter, AmmoPoolCost);
		}
		else
		{
			if (WeaponItem->GetDefaultObject<ABaseWeapon>())
			{
				TmpCharacter->EquipWeapon(WeaponItem, TmpCharacter->GetWeaponSocket());
			}
		}
		TmpCharacter->CurrentWeapon->WeaponReloaded.Broadcast();
		Super::OverlapItem(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	}
}
