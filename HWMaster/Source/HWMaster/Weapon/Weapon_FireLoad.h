// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/BaseWeapon.h"
#include "Weapon_FireLoad.generated.h"

/**
 * 
 */
UCLASS(abstract)
class HWMASTER_API AWeapon_FireLoad : public ABaseWeapon
{
	GENERATED_BODY()
	
protected: 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		float MaxDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Default")
		float CurrentDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		float LoadindSeconds;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		float LoadindUnit = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		class UParticleSystem* ParticleBeam;

	UPROPERTY()
		FTimerHandle LoadingTimer;

	UFUNCTION()
		void SetDamage();

	void StopTimer();

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		class USoundBase* LoadingSound;

public:
	
	virtual void WeaponFire() override;
	
	virtual void StartFire() override;
	void PlayLoadingSound();
	/*
	
	virtual void WeaponReload() override;
	*/
	
	virtual void StopFire() override;

	UPROPERTY(EditDefaultsOnly, Category = "Trace")
		float TraceLength = 500;
	UPROPERTY(EditDefaultsOnly, Category = "Trace")
		float SphereRadius = 50;


};
