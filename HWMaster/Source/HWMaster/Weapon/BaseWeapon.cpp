// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseWeapon.h"
#include "Animation/AnimMontage.h"
#include "Particles/ParticleEmitter.h"
#include "Particles/ParticleSystemComponent.h"
#include "ParticleDefinitions.h"
#include "Engine.h"
#include "MyCharacter.h"
#include "GameFramework/Character.h"
#include "ImpactEffectComponent.h"



ABaseWeapon::ABaseWeapon()
{
	GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetStaticMeshComponent()->SetMobility(EComponentMobility::Movable);

	ImpactEffectComp = CreateDefaultSubobject<UImpactEffectComponent>(TEXT("ImpactEffectComponent"));
}

int ABaseWeapon::AmmoPoolRealod(int &APool, int &MAmmo)
{
	if (APool < MAmmo)
	{
		MAmmo -= APool;

		return APool;
	}
	else
	{
		int TmpAmmo;
		TmpAmmo = MAmmo;
		MAmmo = 0;

		return TmpAmmo;
	}
}


void ABaseWeapon::StartFire()
{
	GetWorldTimerManager().SetTimer(AutoFireTimerTH, this, &ABaseWeapon::WeaponFire, (60 / FireRange), bAutoFire, 0);

}

void ABaseWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(AutoFireTimerTH);
}

void ABaseWeapon::WeaponFire()
{
	if (CurrentAmmo > 0)
	{
		FTransform MuzzleTransform = this->GetStaticMeshComponent()->GetSocketTransform(MuzzleSocketName);
		/*FVector MuzzleSocketLocation = this->GetStaticMeshComponent()->GetSocketLocation(MuzzleSocketName);*/
		
		AmmoChange.Broadcast();

		if (FireAnimation)
		{
			WeaponOwner->PlayAnimMontage(FireAnimation);
			CurrentAmmo--;
		}
		if (EffectParticles)
		{
			UGameplayStatics::SpawnEmitterAtLocation(this, EffectParticles, MuzzleTransform.GetLocation(), MuzzleTransform.GetRotation().Rotator());
		}
		if (EffectSound)
		{
			UGameplayStatics::PlaySoundAtLocation(this, EffectSound, MuzzleTransform.GetLocation());
		}
	}

	AmmoChange.Broadcast();
}

void ABaseWeapon::WeaponReload()
{
	WeaponOwner->PlayAnimMontage(ReloadAnimation);
	CurrentAmmo = AmmoPoolRealod(AmmoPool, MaxAmmo);
	WeaponReloaded.Broadcast();
}

FTransform ABaseWeapon::GetMuzzleTransfor()
{
	return GetStaticMeshComponent()->GetSocketTransform(MuzzleSocketName);
}

void ABaseWeapon::BeginPlay()
{
	Super::BeginPlay();
	CurrentAmmo = AmmoPool;
	AActor * MyOwner = GetOwner();
	if (MyOwner)
	{
		WeaponOwner = Cast<ACharacter>(GetOwner());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("NoOwnerBeginPlay"));
	}
}

