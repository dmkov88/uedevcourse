// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/BaseWeapon.h"
#include "Weapon_Projectile.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class HWMASTER_API AWeapon_Projectile : public ABaseWeapon
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class ABaseProjectile> ProjectileClass;

protected:
	virtual void WeaponFire() override;
	virtual void WeaponReload() override;
	virtual void BeginPlay() override;

};
