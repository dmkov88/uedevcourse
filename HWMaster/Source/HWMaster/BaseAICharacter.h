// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BaseAICharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamAIDelegate);

class UCharacterMovementComponent;

UENUM()
enum AIMovementSpeed
{
	AiWalk = 200,
	AiJog = 600,
	AiSprint = 1000

};

UCLASS()
class HWMASTER_API ABaseAICharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseAICharacter();

	UPROPERTY()
		FTimerHandle AITimerHandle;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void AIMoveWalk();
	void AIMoveJog();
	void AIMoveSprint();

	UFUNCTION()
		void DestroyActor();


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UVitalityComponent* VitalityComponent;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION()
	void AIDieCharacter();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Global state")
	bool bDisableMovement;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Global state")
	bool bIsDead;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
