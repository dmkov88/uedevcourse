// Fill out your copyright notice in the Description page of Project Settings.


#include "ImpactEffectActor.h"
#include "Kismet/GameplayStatics.h"
#include "Math/RotationMatrix.h"

// Sets default values
AImpactEffectActor::AImpactEffectActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AImpactEffectActor::BeginPlay()
{
	Super::BeginPlay();
	SpawnEffects();
}

void AImpactEffectActor::SpawnEffects()
{
	if (DecalMaterial)
	{
		FRotator DecalRotation = FRotationMatrix::MakeFromX(EffectHit.ImpactNormal).Rotator();
		float RandomRoatation = FMath::FRandRange(-180, 180);
		DecalRotation.Roll = RandomRoatation;

		UGameplayStatics::SpawnDecalAttached(DecalMaterial, FVector(6, 12, 12), EffectHit.GetComponent(),
			EffectHit.BoneName, EffectHit.ImpactPoint, DecalRotation, EAttachLocation::KeepWorldPosition, 5.f);
	}

	/*if (bApplyImpulse)
	{
		if (EffectHit.GetComponent()->Mobility == EComponentMobility::Movable)
		{
			if (GetOwner())
			{
				EffectHit.GetComponent()->AddImpulse((EffectHit.ImpactPoint - GetOwner()->GetOwner()->GetActorLocation().GetSafeNormal()) * ImpulseStregth, "None", true);
			}
		}
	}*/
	if (EffectSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, EffectSound, EffectHit.ImpactPoint);
	}
	if (EffectParticles)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, EffectParticles, EffectHit.ImpactPoint, FRotator::ZeroRotator);
	}

	if (EffectHit.GetActor())
	{
		UGameplayStatics::ApplyDamage(EffectHit.GetActor(), Damage, nullptr, GetOwner(), nullptr);
	}
}

// Called every frame
void AImpactEffectActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AImpactEffectActor::HitInint(FHitResult Hit)
{
	EffectHit = Hit;
}

float AImpactEffectActor::GetDamage()
{
	return Damage;
}

