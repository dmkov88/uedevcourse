// Fill out your copyright notice in the Description page of Project Settings.


#include "Booster.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "Components/TextRenderComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "TimerManager.h"



// Sets default values
ABooster::ABooster()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("BOOSTER_Arrow"));
	ArrowComponent->SetupAttachment(RootComponent);

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BOOSTER_BoxCollision"));
	BoxComponent->SetupAttachment(ArrowComponent);

	TextRenderComponent = CreateDefaultSubobject<UTextRenderComponent>(TEXT("BOOSTER_Text"));
	TextRenderComponent->SetupAttachment(ArrowComponent);

	ArrowComponent->bHiddenInGame = false;
	ArrowComponent->bTreatAsASprite = true;
	
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ABooster::OnOverlapBegin);
}


void ABooster::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	
	UE_LOG(LogTemp, Warning, TEXT("Overlaped"));
	
	if (ACharacter* MyCharacter = Cast<ACharacter>(OtherActor))
	{
		UCharacterMovementComponent* OverlapedComponent = MyCharacter->GetCharacterMovement();
		CharSpeed = OverlapedComponent->GetMaxSpeed();
		SetupDefaultSpeed(OverlapedComponent, BoosterSpeed);
		
		FTimerHandle TimerHadle;
		FTimerDelegate BoosterDelegate;
		
		BoosterDelegate.BindUFunction(this, FName("SetupDefaultSpeed"), OverlapedComponent, CharSpeed);
		GetWorld()->GetTimerManager().SetTimer(TimerHadle, BoosterDelegate, SpeedEffectTime, false);
		
	}

}
void ABooster::SetupDefaultSpeed(UCharacterMovementComponent* MovementComponent, float CharacterSpeed)
{
	MovementComponent->MaxWalkSpeed = CharacterSpeed;
}



void ABooster::Tick(float DeltaTime)
{
}


// Called when the game starts or when spawned
void ABooster::BeginPlay()
{
	Super::BeginPlay();

}


