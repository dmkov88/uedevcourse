// Fill out your copyright notice in the Description page of Project Settings.


#include "BasePickUpItem.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "Particles/ParticleEmitter.h"
#include "GameFramework/Character.h"


ABasePickUpItem::ABasePickUpItem()
{
	GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	GetStaticMeshComponent()->SetMobility(EComponentMobility::Static);

	
}


void ABasePickUpItem::OverlapItem(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (Cast<ACharacter>(OtherActor))
	{
		
		if (Particles)
		{
			
			FTransform TmpSpawTransform = OverlappedComp->GetComponentTransform();
			UGameplayStatics::SpawnEmitterAtLocation(this, Particles, SweepResult.ImpactPoint, FRotator::ZeroRotator);

		}
		if (ItemSound)
		{
			UGameplayStatics::PlaySoundAtLocation(this, ItemSound, OverlappedComp->GetComponentLocation());
		}
		Destroy();
	}
}

void ABasePickUpItem::BeginPlay()
{
}

void ABasePickUpItem::OnDestroyed()
{

}
