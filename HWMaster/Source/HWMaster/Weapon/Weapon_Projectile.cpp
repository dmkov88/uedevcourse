// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon_Projectile.h"
#include "BaseProjectile.h"
#include "Engine.h"
#include "ImpactEffectComponent.h"

void AWeapon_Projectile::WeaponFire()
{
	
	if (CurrentAmmo > 0)
	{
		Super::WeaponFire();

		if (ProjectileClass)
		{
			FTransform TmpTransform = GetMuzzleTransfor();
			ABaseProjectile* TmpProjectile = Cast<ABaseProjectile>(GetWorld()->SpawnActor(ProjectileClass, &TmpTransform));

			TmpProjectile->OnHitFire.BindDynamic(ImpactEffectComp, &UImpactEffectComponent::SpawnImpactEffect);
		}
		
	}
}

void AWeapon_Projectile::WeaponReload()
{
	Super::WeaponReload();
}

void AWeapon_Projectile::BeginPlay()
{
	Super::BeginPlay();

}
