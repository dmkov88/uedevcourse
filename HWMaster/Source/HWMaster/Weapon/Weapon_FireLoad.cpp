// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon_FireLoad.h"
#include "CollisionQueryParams.h"
#include "WorldCollision.h"
#include "Private/KismetTraceUtils.h"
#include "Weapon/BaseWeapon.h"
#include "Weapon/ImpactEffectComponent.h"
#include "ImpactEffectActor.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Public/TimerManager.h"


void AWeapon_FireLoad::WeaponFire()
{
	//Super::WeaponFire();
	if (EffectSound)
	{
		FTransform MuzzleTransform = this->GetStaticMeshComponent()->GetSocketTransform(MuzzleSocketName);
		UGameplayStatics::PlaySoundAtLocation(this, EffectSound, MuzzleTransform.GetLocation());
	}

	TArray<FHitResult> Hits;

	FVector TraceStart = GetMuzzleTransfor().GetLocation();
	FVector TraceEnd = GetMuzzleTransfor().GetRotation().GetForwardVector() * TraceLength + TraceStart;

	FCollisionObjectQueryParams CollisionParams;
	CollisionParams.AddObjectTypesToQuery(ECC_PhysicsBody);
	CollisionParams.AddObjectTypesToQuery(ECC_WorldDynamic);
	CollisionParams.AddObjectTypesToQuery(ECC_WorldStatic);

	FCollisionShape CollsionShape = FCollisionShape::MakeSphere(SphereRadius);

	GetWorld()->SweepMultiByObjectType(Hits, TraceStart, TraceEnd, FQuat::Identity, CollisionParams, CollsionShape);

	//DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Red, true);

	if (Hits.Num() > 0)
	{
		for (auto TmpHit : Hits)
		{

			//TmpHit.GetComponent()->AddImpulse(TmpHit.ImpactPoint);

			ImpactEffectComp->SpawnImpactEffect(TmpHit);

			//DrawDebugSphere(GetWorld(), TmpHit.ImpactPoint, SphereRadius, 16, FColor::Blue, true);

		}
	}

	if (ParticleBeam)
	{
		UParticleSystemComponent * BeamComponent = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleBeam, FTransform::Identity);

		BeamComponent->SetBeamSourcePoint(0, TraceStart, 0);
		BeamComponent->SetBeamTargetPoint(0, TraceEnd, 0);
	}
}
void AWeapon_FireLoad::StartFire()
{
	if (ImpactEffectComp)
	{
		if (MaxAmmo > 0)
		{
			GetWorld()->GetTimerManager().SetTimer(LoadingTimer, this, &AWeapon_FireLoad::SetDamage, LoadindSeconds, true, 0.f);
		}
		
	}
}

void AWeapon_FireLoad::PlayLoadingSound()
{
	if (LoadingSound)
	{
		FTransform MuzzleTransform = this->GetStaticMeshComponent()->GetSocketTransform(MuzzleSocketName);
		UGameplayStatics::PlaySoundAtLocation(this, LoadingSound, MuzzleTransform.GetLocation());
	}
}

void AWeapon_FireLoad::SetDamage()
{
	if (CurrentDamage < MaxDamage && MaxAmmo > 0)
	{
		CurrentDamage += LoadindUnit;
		CurrentAmmo = CurrentDamage;
		MaxAmmo -= LoadindUnit;
	}
	else
	{
		//CurrentDamage = MaxDamage;
		PlayLoadingSound();
		StopTimer();
	}
	UE_LOG(LogTemp, Warning, TEXT("%f"), CurrentDamage);
	WeaponReloaded.Broadcast();
	AmmoChange.Broadcast();
}

void AWeapon_FireLoad::StopTimer()
{
	GetWorld()->GetTimerManager().ClearTimer(LoadingTimer);
}

void AWeapon_FireLoad::StopFire()
{
	StopTimer();
	if (CurrentAmmo > 0)
	{
		AImpactEffectActor * ImpactActor = Cast<AImpactEffectActor>(ImpactEffectComp->ImpactActor_Class->GetDefaultObject<AImpactEffectActor>());
		if (ImpactActor)
		{
			ImpactActor->Damage = CurrentDamage;
		}
	
			WeaponFire();

			CurrentDamage = 0;
			CurrentAmmo = CurrentDamage;
			AmmoChange.Broadcast();
	
	}

	
}