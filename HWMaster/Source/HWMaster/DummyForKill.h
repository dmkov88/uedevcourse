// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "DummyForKill.generated.h"

/**
 * 
 */
UCLASS()
class HWMASTER_API ADummyForKill : public AStaticMeshActor
{
	GENERATED_BODY()
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DefaultsConfig")
		int MaxHealth = 100;

public:
	ADummyForKill();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DefaultsConfig")
		int CurrentHealth;

	UFUNCTION()
		void OnCompHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);
};
