// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Booster.generated.h"

class UArrowComponent;
class UBoxComponent;
class UTextRenderComponent;
class UCharacterMovementComponent;



UCLASS()
class HWMASTER_API ABooster : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABooster();


	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UArrowComponent* ArrowComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UBoxComponent* BoxComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UTextRenderComponent* TextRenderComponent;
	
	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void SetupDefaultSpeed(class UCharacterMovementComponent* MovementComponent, float CharacterSpeed);

	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BoosterObjectParametrs")
		float BoosterSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BoosterObjectParametrs")
		float SpeedEffectTime;
	UPROPERTY(BlueprintReadWrite, Category = "BoosterObjectParametrs")
		float CharSpeed;


};
